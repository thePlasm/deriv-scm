(load "avl.scm")

(define
	deriv-list
	'(
		(f . f-prime)))

(define
	(populate-tree lst acc)
	(if
		(null? lst)
		acc
		(populate-tree
			(cdr lst)
			(insert-avl-tree
				acc
				(car lst)
				(lambda
					(x y)
					(symbol<?
						(car x)
						(car y)))))))

(define deriv-tree (populate-tree deriv-list empty-tree))

(define
	(deriv-sum terms var)
	(if
		(null? terms)
		0
		(letrec
			(
				(sum-helper
					(lambda
						(t acc)
						(if
							(null? t)
							acc
							(sum-helper
								(cdr t)
								(cons
									(deriv
										(car t)
										var)
									acc))))))
			(cons '+ (sum-helper terms '())))))

(define
	(deriv-minus expr var)
	(cond
		((null? expr) 0)
		((null? (cdr expr)) (deriv-product (list -1 (car expr)) var))
		(else
			(deriv-sum
				(cons
					(car expr)
					(map
						(lambda
							(x)
							(list '- x))
						(cdr expr)))
				var))))

(define
	(deriv-product factors var)
	(cond
		((null? factors) 0)
		((null? (cdr factors)) (deriv (car factors) var))
		(else
			(list
				'+
				(list
					'*
					(deriv (car factors) var)
					(cons '* (cdr factors)))
				(list
					'*
					(deriv-product (cdr factors) var)
					(car factors))))))

(define
	(deriv-div expr var)
	(cond
		((null? expr) 0)
		((null? (cdr expr))
			(list
				'-
				(list
					'/
					(deriv
						(car expr)
						var)
					(list
						'expt
						(car expr)
						2))))
		(else
			(deriv-product
				(cons
					(map
						(lambda
							(x)
							(list '/ x)))
					(cdr expr))
				var))))

(define
	(deriv-expt expr var)
	(if
		(or
			(null? expr)
			(null?
				(cdr expr)))
		0
		(list
			'*
			(list
				'+
				(list
					'/
					(list
						'*
						(cadr expr)
						(deriv (car expr) var))
					(car expr))
				(list
					'*
					(list
						'log
						(car expr))
					(deriv
						(cadr expr)
						var)))
			(list
				'expt
				(car expr)
				(cadr expr)))))

(define
	(deriv-sqrt expr var)
	(if
		(null? expr)
		0
		(list
			'/
			(deriv (car expr) var)
			(list
				'*
				2
				(list
					'sqrt
					(car expr))))))

(define
	(deriv-exp expr var)
	(if
		(null? expr)
		0
		(list
			'*
			(deriv
				(car expr)
				var)
			(list
				'exp
				(car expr)))))

(define
	(deriv-log expr var)
	(if
		(null? expr)
		0
		(list
			'/
			(deriv (car expr) var)
			(car expr))))

(define
	(deriv-sin expr var)
	(if
		(null? expr)
		0
		(list
			'*
			(deriv
				(car expr)
				var)
			(list
				'cos
				(car expr)))))

(define
	(deriv-cos expr var)
	(if
		(null? expr)
		0
		(list
			'*
			(deriv
				(car expr)
				var)
			(list
				'-
				(list
					'sin
					(car expr))))))

(define
	(deriv-tan expr var)
	(if
		(null? expr)
		0
		(list
			'/
			(deriv (car expr) var)
			(list
				'expr
				(list
					'cos
					(car expr))
				2))))

(define
	(deriv-chain expr func var)
	(list
		'*
		(deriv
			(car expr)
			var)
		(list
			func
			(car expr))))

(define
	(deriv expr var)
	(cond
		((null? expr)
			'())
		((eq? expr var)
			1)
		((not (pair? expr))
			0)
		((pair? (car expr))
			(deriv (car expr) expr))
		(else
			(case
				(car expr)
				((+) (deriv-sum (cdr expr) var))
				((-) (deriv-minus (cdr expr) var))
				((*) (deriv-product (cdr expr) var))
				((/) (deriv-div (cdr expr) var))
				((expt) (deriv-expt (cdr expr) var))
				((sqrt) (deriv-sqrt (cdr expr) var))
				((exp) (deriv-exp (cdr expr) var))
				((log) (deriv-log (cdr expr) var))
				((sin) (deriv-sin (cdr expr) var))
				((cos) (deriv-cos (cdr expr) var))
				((tan) (deriv-tan (cdr expr) var))
				(else
					(cond
						((eq? (car expr) var) 1)
						((null? (cdr expr)) 0)
						(
							(search-tree
								deriv-tree
								(cons (car expr) '())
								(lambda
									(x y)
									(symbol<?
										(car x)
										(car y)))
								(lambda
									(x y)
									(eq?
										(car x)
										(car y))))
							=>
							(lambda
								(x)
								(deriv-chain (cdr expr) (cdr x) var)))
						(else '())))))))