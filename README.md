# deriv-scm

A scheme program that will differentiate an expression with respect to a variable that was inspired by a certain SICP exercise.

# Dependencies

This program requires my AVL scheme library for fast custom derivative lookups.

# Usage

The deriv function takes an expression and a symbol and differentiates the expression with respect to the variable represented by the symbol given.

# TODO

The deriv function does not simplify any of the expressions that it returns, hence it is very verbose. Expression simplification should be implemented.
